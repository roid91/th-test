<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Send your tips for free!</h1>

        <p class="lead">Just login to continue...</p>

        <p><a class="btn btn-lg btn-success" href="<?= Url::to(['site/login'])?>">Login</a></p>
    </div>

</div>
