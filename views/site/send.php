<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\UserTransaction;

$this->title = 'My Yii Application';
?>
<div class="site-send">

        <h1>Send your tips for free!</h1>

		<p>Please fill out the following fields to send:</p>
		
		<p>Your balance is: <?= Yii::$app->formatter->asDecimal($user_balance) ?> tips</p>

		<br>
		
		<?php if (isset($status_id) && $status_id === UserTransaction::STATUS_SEND_SELF) { ?>
		<div class="alert alert-danger" role="alert">
			<strong>FAILED!</strong> You can't send the tips to yourself.
		</div>
		<?php } elseif(isset($status_id) && $status_id === UserTransaction::STATUS_UNKNOWN_ERR) { ?>
		<div class="alert alert-danger" role="alert">
			<strong>FAILED!</strong> Unknown error.
		</div>
		<?php } elseif(isset($status_id) && $status_id > 0) { ?>
		<div class="alert alert-success" role="alert">
			<strong>OK!</strong> The tips was successfully sended.
		</div>
		<?php } ?>
		

		
		
		<?php $form = ActiveForm::begin([
			'id' => 'send-form',
			'layout' => 'horizontal',
			'fieldConfig' => [
				'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
				'labelOptions' => ['class' => 'col-lg-1 control-label'],
			],
		]); ?>

			<?= $form->field($model, 'nickname')->textInput(['autofocus' => true]) ?>
			
			
			<?= $form->field($model, 'tips')->textInput() ?>


			<?= $form->field($model, 'captcha')->widget(Captcha::className(), [
				'template' => '<div class="row"><div class="col-lg-5">{image}</div><div class="col-lg-7">{input} <small>testme</small></div></div>',
			]) ?>
			

			<div class="form-group">
				<div class="col-lg-offset-1 col-lg-11 text-left">
					<?= Html::submitButton('Send', ['class' => 'btn btn-lg btn-success', 'name' => 'send-button']) ?>
				</div>
			</div>

		<?php ActiveForm::end(); ?>


</div>
