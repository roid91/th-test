<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'User Transactions';
?>
<div class="user-transaction-index">
	
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
			[
				'attribute' => 'user_id_from',
				'value' => function($data) {
					return $data->userFrom->nickname . ' ['.$data->userFrom->id.']' . ($data->userFrom->id == Yii::$app->user->id ? ' (You)' : '');
				}
			],
			[
				'attribute' => 'user_id_to',
				'value' => function($data) {
					return $data->userTo->nickname . ' ['.$data->userTo->id.']' . ($data->userTo->id == Yii::$app->user->id ? ' (You)' : '');
				}
			],
			[
				'attribute' => 'amount',
				'value' => function($data) {
					return ($data->userTo->id == Yii::$app->user->id ? '+' : '-') . Yii::$app->formatter->asDecimal($data->amount);
				}
			],
            'date_create',
        ],
    ]); ?>
</div>
