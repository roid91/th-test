<?php
	
namespace app\components\behaviors;

use Yii;
use yii\web\User;

class UserBehavior extends \yii\base\Behavior
{
    public function attach($owner)
    {
		Yii::$app->user->on(User::EVENT_AFTER_LOGIN, [$this, 'afterLogin']);
    }
	
    public function afterLogin($event)
    {
		Yii::$app->user->identity->updateAttributes([
			'last_login' => new \yii\db\Expression('NOW()')
		]);
    }
}
