<?php
	
namespace app\components\behaviors;

use Yii;
use yii\db\ActiveRecord;
use app\models\User;

class UserTransactionBehavior extends \yii\base\Behavior
{
    public function attach($owner)
    {
		$owner->on(ActiveRecord::EVENT_AFTER_INSERT, [$this, 'afterInsert']);
    }
	
	public function afterInsert($event)
	{
		$user_from = User::findOne([
			'id' => $event->sender->user_id_from
		]);
		$user_from->balance = $user_from->balance - $event->sender->amount;
		$user_from->save();
		
		$user_to = User::findOne([
			'id' => $event->sender->user_id_to
		]);
		$user_to->balance = $user_to->balance + $event->sender->amount;
		$user_to->save();
	}
}
