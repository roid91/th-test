<?php

use app\models\User;
use app\models\UserTransaction;

class UserTransactionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        User::updateAll([
            'balance' => 0
        ]);
        
        UserTransaction::deleteAll();
    }

    protected function _after()
    {
    }

    // tests
    public function testSendToNewNickname()
    {
        $user             = new User();
        $user_transaction = new UserTransaction();
        
        $this->assertTrue($user->login('test_user'));
        
        $this->assertGreaterThan(0, $user_transaction->sendToNickname('test_new_user', 10));
        
        $this->tester->seeRecord('app\models\User', [
            'nickname' => 'test_new_user'
        ]);
        
        $user_row = $this->tester->grabRecord('app\models\User', array('nickname' => 'test_new_user'));
        $this->assertSame('10.00', $user_row->balance);
        
        $user_row = $this->tester->grabRecord('app\models\User', array('nickname' => 'test_user'));
        $this->assertSame('-10.00', $user_row->balance);
        
        $this->tester->seeRecord('app\models\UserTransaction', [
            'user_id_from' => $user->findByNickname('test_user')->id,
            'user_id_to'   => $user->findByNickname('test_new_user')->id,
            'amount'       => 10,
        ]);
        
    }
    
    public function testSendToYourself()
    {
        $user             = new User();
        $user_transaction = new UserTransaction();
        
        $this->assertTrue($user->login('test_user'));
        
        $this->assertSame(UserTransaction::STATUS_SEND_SELF, $user_transaction->sendToNickname('test_user', 10));
        
        $user_row = $this->tester->grabRecord('app\models\User', array('nickname' => 'test_new_user'));
        $this->assertSame('0.00', $user_row->balance);
        
        $user_row = $this->tester->grabRecord('app\models\User', array('nickname' => 'test_user'));
        $this->assertSame('0.00', $user_row->balance);
        
        $this->tester->dontSeeRecord('app\models\UserTransaction', [
            'user_id_from' => $user->findByNickname('test_user')->id,
            'user_id_to'   => $user->findByNickname('test_new_user')->id,
            'amount'       => 10,
        ]);
        
    }
}