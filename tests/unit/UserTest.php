<?php

use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public $nickname;
    
    protected function _before()
    {
        $this->nickname = 'test_user';
    }

    protected function _after()
    {
    }

    // tests
    public function testUserRegister()
    {
        $find = User::findOne([
            'nickname' => $this->nickname
        ]);
        if (isset($find))
            $find->delete();
    
        $user = new User();
        
        $this->assertInstanceOf('app\models\User', $user->register($this->nickname));
        
        $this->tester->seeRecord('app\models\User', [
            'nickname' => $this->nickname
        ]);
        
    }
    
    /**
     * @depends testUserRegister
     */
    public function testUserLogin()
    {
        $user = new User();
        
        $this->assertTrue($user->login($this->nickname));
        
        $user_row = $this->tester->grabRecord('app\models\User', array('nickname' => $this->nickname));
        $this->assertNotNull($user_row->last_login);
        
    }
}