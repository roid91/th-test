<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use app\components\behaviors\UserTransactionBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

class UserTransaction extends ActiveRecord
{
    
    // you can't send tips to youself
    const STATUS_SEND_SELF = -1;
    
    // error saving to db
    const STATUS_UNKNOWN_ERR = 0;
    
    // user_from_id - is empty (user not auth)
    const STATUS_EMPTY_USER = -2;
    
    public static function tableName()
    {
        return '{{users_transactions}}';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_create'],
                ],
                'value' => new Expression('NOW()'),
            ],
            
            // update balance db field in users table
            'afterInsert' => UserTransactionBehavior::className(),
        ];
    }
    
    /**
     * Send tips to user by nickname
     * 
     * @param string $nickname 
     * @param double $tips 
     * 
     * @return int
     */
    public function sendToNickname($nickname, $tips)
    {
        if (!Yii::$app->user->isGuest) {
        
            // start db transaction
            $db_transaction = Yii::$app->db->beginTransaction();
            try {
                $user       = new User();
                $user_id_to = null;
                
                $finded = $user->findByNickname($nickname);
                if (isset($finded->id))
                    $user_id_to = $finded->id;
                
                if (!isset($user_id_to)) {
                    // if nickname not found then register it
                    $user_id_to = $user->register($nickname)->id;
                    
                } elseif ($user_id_to == Yii::$app->user->id) {
                    // send tips to youself
                    return self::STATUS_SEND_SELF;
                }
                
                // save
                $this->user_id_from = Yii::$app->user->id;
                $this->user_id_to   = $user_id_to;
                $this->amount       = $tips;
                $this->save();
                
                $db_transaction->commit();
                
            } catch (Exception $e) {
                $db_transaction->rollBack();
            }
            
            return $this->id > 0 ? $this->id : self::STATUS_UNKNOWN_ERR;
            
        } else {
            return self::STATUS_EMPTY_USER;
        }
        
        return self::STATUS_UNKNOWN_ERR;
        
    }
    
    /**
     * Get user object from user_id_to db field
     * 
     * @return \yii\db\ActiveQueryInterface
     */
    public function getUserTo()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id_to']);
    }
    
    /**
     * Get user object from user_id_from db field
     * 
     * @return \yii\db\ActiveQueryInterface
     */    
    public function getUserFrom()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id_from']);
    }
    
}
