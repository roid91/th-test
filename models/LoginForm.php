<?php

namespace app\models;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends \yii\base\Model
{
    public $nickname;
    public $captcha;
    public $rememberMe = true;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // nickname and captcha are both required
            [['nickname', 'captcha'], 'required'],
            
            [['nickname'], 'filter', 'filter' => 'trim'],
            ['nickname', 'string', 'min' => 2, 'max' => 255],
            
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            
            // captcha needs to be entered correctly
            ['captcha', 'captcha'],
        ];
    }

    /**
     * Logs in a user using the provided nickname.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
        
            $user = new User();
            if (!($logined_user = $user->login($this->nickname, $this->rememberMe))) {
                $logined_user = $user->register($this->nickname, true, $this->rememberMe);
            }
            
            return $logined_user;
        }
        
        return false;
    }
    
}
