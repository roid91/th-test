<?php

namespace app\models;

use Yii;
use app\models\LoginForm;
use app\models\User;
use app\models\UserTransaction;

/**
 * SendForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class SendForm extends \yii\base\Model
{

    public $nickname;
    public $tips;
    public $captcha;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // nickname and captcha are both required
            [['nickname', 'captcha', 'tips'], 'required'],
            
            [['nickname'], 'filter', 'filter' => 'trim'],
            ['nickname', 'string', 'min' => 2, 'max' => 255],
            
            ['tips', 'double', 'min' => 0.01],
            
            // captcha needs to be entered correctly
            ['captcha', 'captcha'],
        ];
    }
    /**
     * Send tips
     * 
     * @return int
     */
    public function send()
    {
        $userTransaction = new UserTransaction();
        
        if ($this->validate()) {
            
            return $userTransaction->sendToNickname($this->nickname, $this->tips);
        }
        
        return $userTransaction::STATUS_UNKNOWN_ERR;
    }

}
