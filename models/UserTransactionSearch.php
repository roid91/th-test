<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserTransaction;

/**
 * UserTransactionSearch represents the model behind the search form about `app\models\UserTransaction`.
 */
class UserTransactionSearch extends UserTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id_from', 'user_id_to'], 'integer'],
            [['amount'], 'number'],
            [['date_create'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserTransaction::find()
        ->where('[[user_id_from]] = :my_user_id OR [[user_id_to]] = :my_user_id', [
            ':my_user_id' => Yii::$app->user->id
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id_from' => $this->user_id_from,
            'user_id_to' => $this->user_id_to,
            'amount' => $this->amount,
            'date_create' => $this->date_create,
        ]);

        return $dataProvider;
    }
}
