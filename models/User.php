<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use app\components\behaviors\UserBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    
    // auth cookie time in sec.
    const REMEMBER_TIME = 3600*24*30; // 1 month
    
    public static function tableName()
    {
        return '{{users}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['date_create'],
                ],
                'value' => new Expression('NOW()'),
            ],
            // update last_login db field
            'userAfterLogin' => UserBehavior::className(),
        ];
    }
    
    /**
     * Finds user by nickname
     *
     * @param string $nickname
     * @return static|null
     */
    public static function findByNickname($nickname)
    {
        return static::findOne([
            'nickname' => $nickname,
        ]);
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
        {
        return static::findOne([
            'id' => $id,
        ]);
    }
    
    /**
     * Finds an identity by the given token.
     *
     * @param string $token the token to be looked for
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
    }
    
    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }
    
    /**
     * Generate auth_key for cookie login
     * @return void
     */
    public function generateAuthKey()
    {
        $this->auth_key = \Yii::$app->security->generateRandomString();
    }
    
    /**
     * Login user by nickname
     * 
     * @param string $nickname 
     * @param bool $remember  true for cookie remember
     * 
     * @return bool
     */
    public function login($nickname, $remember = false)
    {
        $user_identify = $this->findByNickname($nickname);
        
        if (isset($user_identify))
            return \Yii::$app->user->login($user_identify, $remember ? self::REMEMBER_TIME : 0);
        else
            return false;
    }    
    
    /**
     * Register user by nickname
     * 
     * @param string $nickname 
     * @param bool $login login after register if it's true
     * @param bool $remember true for cookie remember
     * 
     * @return User|bool
     */
    public function register($nickname, $login = false, $remember = false)
    {
        $this->nickname = $nickname;
        $this->generateAuthKey();
        
        if ($this->save()) {
            if ($login)
                return $this->login($nickname, $remember);
            
            return $this;
        }
            
        return false;
    }
    
}
