<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_transactions`.
 */
class m170624_141251_create_users_transactions_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users_transactions', [
            'id'           => $this->primaryKey(),
            'user_id_from' => $this->integer()->notNull(),
            'user_id_to'   => $this->integer()->notNull(),
            'amount'       => $this->decimal(10, 2)->notNull(),
            'date_create'  => $this->datetime()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users_transactions');
    }
}
