<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170624_141145_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id'          => $this->primaryKey(),
            'nickname'    => $this->string(255)->unique()->notNull(),
            'balance'     => $this->decimal(10, 2)->defaultValue(0),
            'auth_key'    => $this->string(32)->notNull(),
            'date_create' => $this->datetime()->notNull(),
            'last_login'  => $this->datetime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
